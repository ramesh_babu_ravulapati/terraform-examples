provider "aws" {
    access_key=""
    secret_key=""
    region="ap-south-1"
}
resource "aws_vpc" "myvpc-123"{
    cidr_block="190.160.0.0/16"
    vpc_id="${aws_vpc.myvpc-123.id}"
}
resource "aws_subnet" "subnet-1"{
    vpc_id="${aws_vpc.myvpc123.id}"
    cidr_block="190.160.1.0/24"
}
resource "aws_subnet" "subnet-2"{
    vpc_id="${aws_vpc.myvpc123.id}"
    cidr_block="190.160.2.0/24"
}
resource "aws_subnet" "subnet-3"{
    vpc_id="${aws_vpc.myvpc123.id}"
    cidr_block="190.160.3.0/24"
}
resource "aws_security_group" "allow-all"{
    name="allow-all"
    decription="allow all inbound traffic"
    vpc_id="${aws_vpc.myvpc123.id}"
    ingress{
        from_port=0
        to_port=0
        protocol="-1"
        cidr_blocks=["0.0.0.0/0"]
    }
    egress{
        from_port=0
        to_port=0
        protocol="-1"
        cidr_blocks=["0.0.0.0/0"]        
    }
}
resource "aws_instance" "web-server"{
    ami=""
    instance_type="t2.micro"
    subnet_id="${aws_subnet.subnet-1.id}"
    vpc_security_group_ids=["${aws_security_group.allow-all.id}"]
    associate_public_ip_address=true
}